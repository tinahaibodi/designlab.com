---
name: Checkboxes
related:
  - dropdowns
  - radio-buttons
  - segmented-control
  - toggles
---

A checkbox is an input which acts as a boolean. Additionally it can have an indeterminate state.

The indeterminate state represents a situation where a list of sub-options is grouped under a parent option and sub-options are in both selected and unselected states.

## Usage

Use a single checkbox when there is a choice between two selections, it has a default state, the results are **not** effective or noticeable immediately, and the user needs a confirmation of it being saved.

Use multiple checkboxes to allow users to select multiple from a list of options, including all or none.

Checkboxes may replace toggles, segmented controls, or radio buttons to allow users to select one, two, or multiple options. For help with choosing the right solution, follow the table below.

Todo: Add replacement-comparison-table

### Labels

Checkbox labels are set in regular font weight, positioned to the right of the element, and should be as short as possible. If more, explanatory text is required, it should be added and styled as [help text](/components/forms#help-text) below the label.

### Visual Design

Checkboxes should use high-contrast colors to indicate the states - true or false. A checkmark and hyphen icon are used to differentiate between true or indeterminate.

### Interaction

Users are able to select an option by clicking on either the checkbox itself or on its label.

## Demo

Todo: Add live component block with code example

## Design specifications

Color, spacing, dimension, and layout specific information pertaining to this component can be viewed using the following link:

[Sketch Measure Preview for checkboxes](https://gitlab-org.gitlab.io/gitlab-design/hosted/design-gitlab-specs/checkboxes-spec-previews/)
